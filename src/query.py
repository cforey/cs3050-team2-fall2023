import re
import string
import firebase_admin
from typing import List
from utils.query_ast import *
from utils.constants import *
from api import filter_compound, count_compound, printDocs
import os
from firebase_admin import credentials

### Debug setup
DEBUG_MODE = False
def print_debug(msg):
    '''Print debug information.
    
    Calls to this function prints a specified debug message if global constant `DEBUG_MODE` is set to `True`
    
    '''

    if DEBUG_MODE:
        print('(DEBUG) ' + str(msg))

### Error-handling setup
class InvalidInputError(Exception):
    '''Custom exception for handling invalid input values.'''
    def __init__(self, message):
        super().__init__(message)

### Parser definitions
HELP_MESSAGE = '''
Available commands:
- `FILTER <condition> [AND <condition> [AND ...]]`: Display all entries satisfying all conditions given.
- `COUNT <condition> [AND condition [AND ...]]`: Display the number of entries satisfying all conditions given.
- `HELP`: Print this help message.
- `QUIT`: Exit the query program.

Conditions take the form `<value> <op> <value>` where:
- Values are a value literal:
    - signed numeric value (integer or float),
    - string enclosed in double quotes ("..."),
    - `null` to represent missing data,
    - field name for a collection
- Operators may be any of the following comparisons:
    - `=` (not `==`)
    - `!=`
    - `>`
    - `<`
    - `>=`
    - `<=`

Available fields:
- country
- temperature_fahrenheit
- wind_mph
- precipitation_in
- humidity
- air_qualities
- visibility_miles
- uv_index
- carbon_monoxide
- nitrogen_dioxide
- sulphur_dioxide
- pm2.5
- pm10
- use_epa_index
- gb_defra_index
'''

def get_query_input() -> str:
    '''Gets user input from the terminal.'''
    query = input('> ').strip()

    #print_debug('get_query_input: \'' + query + '\'')
    return query

def validate_characters(query: str):
    '''Validates the entire input based on characters present.

    Valid characters include all ASCII letters A through Z (upper/lowercase), digits 0-9, and symbols needed for comparison operators, punctuation, and string delimitation.
    
    This function returns nothing and throws an `InvalidInputError` if any invalid characters are passed as an input.
    
    '''

    valid_characters = set('\"\t =><!.-_' + string.ascii_letters + string.digits)

    if set(query).issubset(valid_characters):
        #print_debug('validate_characters: input is valid')
        return
    else:
        #print_debug('validate_characters: input is invalid')
        raise InvalidInputError('Invalid characters used in input')

def tokenize(query: str) -> List[str]:
    '''Tokenises based on spaces and string delimiters.
    
    Groups of characters separated by spaces will be turned into tokens unless a string delimiter has been opened, in which case all subsequent whitespace will be placed in the token (as opposed to being skipped). Note that this function does not strictly assess the balance of string delimiters. This implementation causes all operators to require a whitespace separation as well (e.g., `country="Iraq"` is invalid, but `country = "Iraq"` is valid).
    
    This function returns a list of strings representing tokens.

    '''

    tokens = []
    inside_string = False
    token_buffer = ''
    
    # Compute tokens
    for char in query:
        match char:
            case ' ' if inside_string:
                token_buffer += ' '
            case ' ':
                if len(token_buffer) > 0:
                    tokens.append(token_buffer)
                    token_buffer = ''  # Reset buffer
            case c if c == STRING_DELIMITER and inside_string:
                inside_string = False
                tokens.append(STRING_DELIMITER + token_buffer + STRING_DELIMITER)
                token_buffer = ''  # Reset buffer
            case c if c == STRING_DELIMITER:
                inside_string = True
            case _:
                token_buffer += char
    
    # Flush buffer
    if token_buffer != '':
        tokens.append(token_buffer)

    #print_debug('tokenizer: ' + str(tokens))
    return tokens

def parse(tokens: List[str]) -> QueryCommand:
    '''Parse tokens into an abstract syntax tree (AST).

    This function uses four layers of functions to parse inputs:
    1. Command (Top-level): Parses the first token to determine what command to use with `parse_command()`.
    2. Compound conditions: Parses a list of atomic conditions separated by the `AND` keyword with `parse_compound_condition()`.
    3. Atomic condition: Parses a single condition with a left-hand and right-hand value separated by a comparison operator with `parse_atomic_condition()`.
    4. Operator/atomic value: Parses a token representing either a value (numeric, string, data field name, &c.) or a comparison operator (=, >, <, &c.).

    This function returns a `QueryCommand` instance representing a command to execute (see `query_ast.py` for available options) and acting as a root node for a query AST. This `QueryCommand` instance contains references to a list of conditions, which in turn contains references to values and operators.
    
    '''

    def parse_command(tokens: List[str]) -> QueryCommand:
        command_token = tokens[0]  # Separate first (command) token

        match command_token.upper():
            case 'QUIT':
                #print_debug('parse_command: Found command `QUIT`')
                return Quit()
            case 'HELP':
                #print_debug('parse_command: Found command `HELP`')
                return Help()
            case 'FILTER':
                if len(tokens) == 1:
                    raise InvalidInputError('Missing arguments for `FILTER`')
                #print_debug('parse_command: Found command `FILTER`')
                return Filter(parse_compound_condition(tokens[1:]))
            case 'COUNT':
                if len(tokens) == 1:
                    raise InvalidInputError('Missing arguments for `COUNT`')
                #print_debug('parse_command: Found command `COUNT`')
                return Count(parse_compound_condition(tokens[1:]))
            case _:
                raise InvalidInputError(f'Command `{command_token}` not found')
    
    def parse_compound_condition(tokens: List[str]) -> List[Condition]:
        conditions = []

        condition_buffer = []

        for t in tokens:
            match t.upper():
                case 'AND':
                    conditions.append(parse_atomic_condition(condition_buffer))
                    condition_buffer.clear()
                case _:
                    condition_buffer.append(t)
        
        # Flush final condition
        conditions.append(parse_atomic_condition(condition_buffer))

        #print_debug(f'parse_compound_condition: Parsed {len(conditions)} conditions')
        return conditions
    
    def parse_atomic_condition(tokens: List[str]) -> Condition:
        # This is not the most robust solution ever, but it will do
        if len(tokens) < 3:
            raise InvalidInputError(f'Conditional expected 3 arguments, {len(tokens)} provided')
        
        condition = Condition(parse_value(tokens[0]),
                         parse_operator(tokens[1]),
                         parse_value(tokens[2]))

        #print_debug(f'parse_atomic_condition: Parsed condition \'{tokens[0]} {tokens[1]} {tokens[2]}\'')
        return condition
    
    def parse_value(value: str) -> Value:
        if value == NULL_VALUE:
            return Null()
        elif re.match('[-+]?[0-9]*.[0-9]+', value):
            return Float(float(value))
        elif re.match('[-+]?[0-9]+', value):
            return Integer(int(value))
        elif re.match('".*"', value):
            return String(value)
        elif value in TOP_LEVEL_FIELD_NAMES:
            return FieldName(TOP_LEVEL_COLLECTION_NAME, value)
        elif value in SUB_LEVEL_FIELD_NAMES:
            return FieldName(SUB_LEVEL_COLLECTION_NAME, value)
        else:
            return String(value)
            #raise InvalidInputError(f'Unable to parse value "{value}"')
    
    def parse_operator(op: str) -> ComparisonOperator:
        if op in COMPARISON_OPERATORS:
            return ComparisonOperator(op)
        raise InvalidInputError(f'Comparison operator "{op}" not found')
    
    # Main body of `parse()`
    command = parse_command(tokens)

    #print_debug('parse: Computed AST: ' + str(command))
    return command

if __name__ == '__main__':
    while True:
        # Get command line input
        query = get_query_input()

        # Validate and construct query AST
        try:
            validate_characters(query)
            tokens = tokenize(query)
            command = parse(tokens)
        except InvalidInputError as e:
            print(f'InvalidInputError: {e}')
            continue  # Restart if input is validation


        match command:
            case Quit():
                break
            case Help():
                print(HELP_MESSAGE)
                continue
            case Filter():
                # Store results
                # Make API calls to datastore
                #query_result = filter_compound(command.conditions)

                # Print results
                printDocs(filter_compound(command.conditions))

            case Count():
                # Collate results
                # Print results
                printDocs(count_compound(command.conditions))
     

