import firebase_admin
from firebase_admin import credentials, initialize_app, firestore
import json
from utils.data_representations import BasicWeather, AirQuality
from utils.constants import *
import sys
#from google.cloud import firestore
from google.cloud.firestore_v1 import aggregation
from google.cloud.firestore_v1.base_query import FieldFilter

num_data_entries = 30 #the number of data entries we will add to speed up the process
max_data_entries = -1 #adding every country ; will take more time.

DEBUG_MODE = True
def print_debug(msg):
    if DEBUG_MODE:
        print('(DEBUG) ' + str(msg))

# Connects to Firebase
def connect():
    # Get path to credentials file
    json_path = CREDENTIALS_PATH

    #we don't want to initialize the app multiple times so this will check if it is not running
    if not firebase_admin._apps:
        #if not running, get the credentials from the json path, and initialize the app
        cred = firebase_admin.credentials.Certificate(json_path) #connection to sdk json file for privacy
        firebase_admin.initialize_app(cred) #initializing app

    db = firestore.client()
    #print_debug("Connected to Firestore")
    

    weather_ref = db.collection(TOP_LEVEL_COLLECTION_NAME)
    air_quality_ref = db.collection(SUB_LEVEL_COLLECTION_NAME)
    
    return weather_ref, air_quality_ref #return both collections, for basic_weather and for environmental



# Reads in data from JSON file
def read_data(file_name):
    relative = DATA_PATH + file_name
    with open(relative,'r') as json_weather: #opening json file with data
        weather = json.load(json_weather)#loading data into weather
    return weather[:num_data_entries]
#read_data()

#deletes data
def delete_collection(coll_ref, batch_size):
    docs = coll_ref.list_documents(page_size=batch_size)#get every document from all collections
    deleted = 0

    for doc in docs: #iterate through each document in docs
        #print(f"Deleting doc {doc.id} => {doc.get().to_dict()}")
        doc.delete()#delete each document
        deleted += 1

    if deleted >= batch_size:
        return delete_collection(coll_ref, batch_size)

# Inserts 'new' same data into firebase
def insert_data(weather, weather_ref, air_quality_ref):
    for weather_instance in weather: #for each data instance
        air_qualities = []#7 air qualities associated with each country
        for i in range(7):#to account for a weather instance for each day of the week
            string = 'air_quality/'+weather_instance['country']+str(i+1)
            air_qualities.append(string)
        #add basic weather in class representation
        basic_weather = BasicWeather(country=weather_instance['country'],
                                      temperature_fahrenheit=weather_instance['temperature_fahrenheit'],
                                      wind_mph=weather_instance['wind_mph'],
                                      precipitation_in=weather_instance['precipitation_in'],
                                      humidity=weather_instance['humidity'],
                                      air_qualities=air_qualities)
        #adds documents to the weather ref as a dictionary
        weather_ref.document(weather_instance['country']).set(basic_weather.to_dict())
        #have some element in weather_ref to essentially point to the associated air quality
        for i in range (7):#to account for an air quality instance each day
            uid = str(weather_instance['country']+str(i+1))#creates unique identifier based on country name plus the day of the week (1-7)
            if(weather_instance['air_qualities'] is not None):
                air_quality = AirQuality(visibility_miles=weather_instance['air_qualities'][i]['visibility_miles'],
                                          uv_index=weather_instance['air_qualities'][i]['uv_index'],
                                          carbon_monoxide=weather_instance['air_qualities'][i]['carbon_monoxide'],
                                          nitrogen_dioxide=weather_instance['air_qualities'][i]['nitrogen_dioxide'],
                                          sulphur_dioxide=weather_instance['air_qualities'][i]['sulphur_dioxide'],
                                          pm2_5=weather_instance['air_qualities'][i]['pm2.5'],
                                          pm10=weather_instance['air_qualities'][i]['pm10'],
                                          us_epa_index=weather_instance['air_qualities'][i]['us_epa_index'],
                                          gb_defra_index=weather_instance['air_qualities'][i]['gb_defra_index'],
                                          uuid = uid)
                air_quality_ref.document(str(weather_instance['country']+str(i+1))).set(air_quality.to_dict())
    #setting air quality data to the collection as a dictionary
    print("new data inserted")

    # TODO: Main function that reads json, connects to firebase, clears database, and inserts new data
if __name__=='__main__':
    # take in the file name from command line argument
    file_name = sys.argv[1]
    # read the file with the given name
    weather_data = read_data(file_name)
    # connect to the firebase and return the references
    weather_ref, air_quality_ref  = connect()
    # delete what is stored in the weather reference
    delete_collection(weather_ref,128)
    print("Deleting Weather")
    # delete what is in the air quality
    delete_collection(air_quality_ref,128)
    print("Deleting Air Quality")
    # insert the data based off of the read file into weather and air quality
    insert_data(weather_data, weather_ref, air_quality_ref)
    # open the query function when this all finishes
    with open("./query.py") as f:
        exec(f.read())

