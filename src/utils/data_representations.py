# Class representations for collection documents
class BasicWeather:
    def __init__(self, country, temperature_fahrenheit, wind_mph, precipitation_in, humidity, air_qualities):
        self.country = country
        self.temperature_fahrenheit = temperature_fahrenheit
        self.wind_mph = wind_mph
        self.precipitation_in = precipitation_in
        self.humidity = humidity
        self.air_qualities = air_qualities
    
    def to_dict(self):
            return {'country' : self.country,
            'temperature_fahrenheit' : self.temperature_fahrenheit,
            'wind_mph' : self.wind_mph,
            'precip_in' : self.precipitation_in,
            'humidity' : self.humidity,
            'associated_air_qualities' : self.air_qualities}
        
    def __repr__(self):
        return f"BasicWeather(\
                country={self.country}, \
                tempertature_fahrenheit={self.temperature_fahrenheit}, \
                wind_mph={self.wind_mph}, \
                precipitation_in={self.precipitation_in}, \
                humidity={self.humidity}, \
                air_qualities={self.air_qualities}\
            )"

#new class for environmental factors
class AirQuality:
        def __init__(self, visibility_miles, uv_index, carbon_monoxide,nitrogen_dioxide, sulphur_dioxide, pm2_5, pm10, us_epa_index, gb_defra_index,uuid):
            self.visibility_miles = visibility_miles
            self.uv_index = uv_index
            self.carbon_monoxide = carbon_monoxide
            self.nitrogen_dioxide = nitrogen_dioxide
            self.sulphur_dioxide = sulphur_dioxide
            self.pm2_5 = pm2_5 #error with period in name
            self.pm10 = pm10
            self.us_epa_index = us_epa_index
            self.gb_defra_index = gb_defra_index
            self.uuid = uuid
        
        def to_dict(self):
            return {'visibility_miles' : self.visibility_miles,
                    'uv_index' : self.uv_index,
                    'carbon_monoxide' : self.carbon_monoxide,
                    'nitrogen_dioxide' : self.nitrogen_dioxide,
                    'sulphur_dioxide' : self.sulphur_dioxide,
                    'pm2_5' : self.pm2_5,
                    'pm10' : self.pm10,
                    'us_epa_index' : self.us_epa_index,
                    'gb_defra_index' : self.gb_defra_index,
                    'uuid' : self.uuid}
        
        def __repr__(self):
            return f"BasicWeather(\
                visibility_miles={self.visibility_miles}, \
                uv_index={self.uv_index}, \
                carbon_monoxide={self.carbon_monoxide}, \
                nitrogen_dioxide={self.nitrogen_dioxide}, \
                sulphur_dioxide={self.sulphur_dioxide}, \
                pm2_5={self.pm2_5}, \
                pm10={self.pm10}, \
                us_epa_index={self.us_epa_index}, \
                gb_defra_index={self.gb_defra_index},\
                uuid={self.uuid}\
            )"
