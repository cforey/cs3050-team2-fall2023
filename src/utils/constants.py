TOP_LEVEL_COLLECTION_NAME = 'basic_weather'
TOP_LEVEL_FIELD_NAMES = ['country', 'temperature_fahrenheit', 'wind_mph', 'precipitation_in', 'humidity', 'air_qualities']

SUB_LEVEL_COLLECTION_NAME = 'air_quality'
SUB_LEVEL_FIELD_NAMES = ['visibility_miles', 'uv_index', 'carbon_monoxide', 'nitrogen_dioxide', 'sulphur_dioxide', 'pm2.5', 'pm10', 'us_epa_index', 'gb_defra_index']

STRING_DELIMITER = '"'

COMPARISON_OPERATORS = {'=', '>', '<', '>=', '<=', '!='}

NULL_VALUE = 'null'

CREDENTIALS_PATH = r'utils/cred.json'
DATA_PATH = r'../data/'
