library(pacman)
pacman::p_load(tidyverse, skimr, jsonlite, purrr)

## Data viewing cheatsheet:
# - spec
# - glimpse
# - skim
# - View

# Import raw data
raw_data <- read_csv("data/weather_data_raw.csv")

# Basic metrics data (top-level)
weather_data <- raw_data %>%
  group_by(country) %>%
  summarise(
    temperature_fahrenheit = mean(temperature_fahrenheit),
    wind_mph = mean(wind_mph),
    precipitation_in = mean(precip_in),
    humidity = mean(humidity)
  )

# Air quality data (sub-level)
air_quality_data <- raw_data %>%
  rename(
    visibility_miles = visibility_miles,
    uv_index = uv_index,
    carbon_monoxide = air_quality_Carbon_Monoxide,
    ozone = air_quality_Ozone,
    nitrogen_dioxide = air_quality_Nitrogen_dioxide,
    sulphur_dioxide = air_quality_Sulphur_dioxide,
    pm2.5 = air_quality_PM2.5,
    pm10 = air_quality_PM10,
    us_epa_index = `air_quality_us-epa-index`,
    gb_defra_index = `air_quality_gb-defra-index`
  ) %>%
  mutate(
    air_quality = pmap(
      list(
        visibility_miles,
        uv_index,
        carbon_monoxide,
        nitrogen_dioxide,
        sulphur_dioxide,
        pm2.5,
        pm10,
        us_epa_index,
        gb_defra_index
      ),
      ~list(
        visibility_miles = ..1,
        uv_index = ..2,
        carbon_monoxide = ..3,
        nitrogen_dioxide = ..4,
        sulphur_dioxide = ..5,
        pm2.5 = ..6,
        pm10 = ..7,
        us_epa_index = ..8,
        gb_defra_index = ..9
      )
    )
  ) %>%
  select(country, air_quality) %>%
  group_by(country) %>%
  summarise(
    air_qualities = list(air_quality)
  )

# Append air_quality column to weather data
weather_data <- weather_data %>% left_join(
  air_quality_data,
  by = "country"
)

# Randomly insert null values into data
set.seed(42)  # for reproducibility
stopifnot(
  nrow(air_quality_data) == nrow(weather_data)  # detect indexing issues
)

for (i in 1:nrow(weather_data)) {
  # Note: tibbles/df's are 1-indexed

  random_value <- runif(1)

  p_nullify_humidity <- 0.05
  p_nullify_air_qualities <- 0.15

  # randomly nullify `humidity` values
  if (random_value < p_nullify_humidity) {
    weather_data$humidity[i] <- NA
  }
  # randomly nullify `air_quality` object data
  if (random_value < p_nullify_air_qualities) {
    weather_data$air_qualities[i] <- NA
  }
}

# View(weather_data)

# Output JSON files
outputJSON <- toJSON(
  weather_data,
  pretty = TRUE,
  auto_unbox = TRUE,
  null = "null",
  na = "null",
)

print(outputJSON)

write(outputJSON, "data/weather_data_clean.json")

rm(list=ls())
