from dataclasses import dataclass
from typing import List

class BaseQueryNode:
    pass

class QueryCommand(BaseQueryNode):
    pass

class Condition(BaseQueryNode):
    pass

class Value(BaseQueryNode):
    pass

@dataclass(frozen=True, eq=True)
class ComparisonOperator(BaseQueryNode):
    op: str

class Quit(QueryCommand):
    pass

class Help(QueryCommand):
    pass

@dataclass(frozen=True, eq=True)
class Filter(QueryCommand):
    conditions: List[Condition]

@dataclass(frozen=True, eq=True)
class Count(QueryCommand):
    conditions: List[Condition]

@dataclass(frozen=True, eq=True)
class Condition(Condition):
    lhs: Value
    op: ComparisonOperator
    rhs: Value

@dataclass(frozen=True, eq=True)
class Integer(Value):
    value: int

@dataclass(frozen=True, eq=True)
class Float(Value):
    value: float

@dataclass(frozen=True, eq=True)
class String(Value):
    value: str

class Null(Value):
    pass

@dataclass(frozen=True, eq=True)
class FieldName(Value):
    collection: str
    field: str