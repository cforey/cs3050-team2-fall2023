import firebase_admin
from google.cloud import firestore
from google.cloud.firestore_v1 import aggregation
from google.cloud.firestore_v1.base_query import FieldFilter
from utils.constants import *
from firebase_admin import credentials, firestore
import os
from admin import connect

DEBUG_MODE = True
def print_debug(msg):
    if DEBUG_MODE:
        print('(DEBUG) ' + str(msg))


### Query Functions ###

# Filter compound boolean conditions.
def filter_compound(conditions):
    # These lists will store the field names, operators, and values for each condition of the compound query. The length of each list will equal the number of conditions
    field_names = []
    operators = [] 
    values = []  

    # Extract the field names, operators, and values from each condition
    for condition in conditions:
            field_names.append(condition.lhs.field)
            
            # If the operator is "=", change it to "==" so firestore will accept it
            if condition.op.op == "=":
                operators.append("==")
            else:
                operators.append(condition.op.op)

            values.append(condition.rhs.value)

    # Create the collection reference objects
    weather_ref, air_quality_ref = connect() 

    # If the field is from the top level collection, use weather_ref. else, use air_quality_ref
    if conditions[0].lhs.collection == TOP_LEVEL_COLLECTION_NAME:
        query = weather_ref
    else:
        query = air_quality_ref
      
    # Append the query conditions
    for i in range(len(field_names)): query = query.where(filter=FieldFilter(field_names[i],operators[i],values[i]))
    
    # Execute the query
    docs = query.get()
    return docs

# Count entries matching a compound boolean condition.
def count_compound(conditions):
    # These lists will store the field names, operators, and values for each condition of the compound query. The length of each list will equal the number of conditions
    field_names = []
    operators = [] 
    values = []  

    # Extract the field names, operators, and values from each condition
    for condition in conditions:
            field_names.append(condition.lhs.field)

            # If the operator is "=", change it to "==" so firestore will accept it
            if condition.op.op == "=":
                operators.append("==")
            else:
                operators.append(condition.op.op)

            values.append(condition.rhs.value)

    # Create the collection reference objects
    weather_ref, air_quality_ref = connect() 

    # If the field is from the top level collection, use weather_ref. else, use air_quality_ref
    if conditions[0].lhs.collection == TOP_LEVEL_COLLECTION_NAME:
        query = weather_ref
    else:
        query = air_quality_ref
   
    # Append the query conditions
    for i in range(len(field_names)): query = query.where(filter=FieldFilter(field_names[i],operators[i],values[i]))

    # Execute the query
    aggregate_query = aggregation.AggregationQuery(query)
    aggregate_query.count()
    result = aggregate_query.get()

    # Return only the value of the aggregation (number of counted documents)
    return result[0][0].value

# Print output from query
def printDocs(docs):
    # If output is an int, print the int
    if isinstance(docs, int):
        print(docs)
    # If output is an empty list, print no results
    elif docs == []:
        print("No Results")
    # Output is a list of documents, so loop through and print each one
    else:
        for doc in docs:
             dict = doc.to_dict()
             
             print("- " + doc.id + "\nHumidity: " + str(dict["humidity"]) + "\nTemperature: " + str(dict["temperature_fahrenheit"]) + " F\nWind: " + str(dict["wind_mph"]) + " mph\nPrecipitation: " + str(dict["precip_in"]) + " inches\n")




